;; Functions on two-dimensional grids
;; Liam Healy 2018-11-03 11:31:58EDT matrix.lisp
;; Time-stamp: <2018-11-03 11:36:45EDT matrix.lisp>

;; Copyright 2009, 2010, 2012, 2016, 2018 Liam M. Healy
;; Distributed under the terms of the GNU General Public License
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :grid)

(export '(row rows column columns matrix-from-columns map-columns
	  matrix-from-rows map-rows))

;;;;****************************************************************************
;;;; Rows and columns of a matrix
;;;;****************************************************************************

(defun row (grid index &optional destination)
  "The subgrid with the first index set to the specified value."
  (codimension-one-subspace grid 0 index destination))

(defun rows (grid)
  "A list of all the rows."
  (loop for index from 0 below (dim0 grid)
	collect (row grid index)))

(defun (setf row) (subgrid grid index)
  "Set the subgrid with the first index set to the specified value."
  (setf (codimension-one-subspace grid 0 index)
	subgrid))

(defun column (grid index &optional destination)
  "The subgrid with the second index set to the specified value."
  (codimension-one-subspace grid 1 index destination))

(defun columns (grid)
  "A list of all the columns."
  (loop for index from 0 below (dim1 grid)
	collect (column grid index)))

(defun (setf column) (subgrid grid index)
  "Set the subgrid with the second index set to the specified value."
  (setf (codimension-one-subspace grid 1 index)
	subgrid))

;;;;****************************************************************************
;;;; Make/map matrix from row or column vectors
;;;;****************************************************************************

(defun matrix-from-columns (&rest columns)
  "Make the matrix out of the equal-length vectors.
   If *default-grid-type* is non-nil, resultant grid
   will be of the specified type; otherwise, it will
   be the same as the first column."
  (assert 
   (apply '=
	  (mapcar
	   (lambda (c) (when (= 1 (length (dimensions c))) (dim0 c)))
	   columns))
   (columns)
   "Columns must be vectors of equal length.")
  (let ((ans (make-grid
	      (make-specification
	       (or (gridp (first columns)) *default-grid-type*)
	       (list (dim0 (first columns)) (length columns))
	       (element-type (first columns))))))
    (loop for j from 0
       for col in columns
       do (setf (column ans j) col))
    ans))

(defun map-columns (function matrix)
  "Apply a function that returns a column for each of the columns of the matrix"
  (apply #'matrix-from-rows (mapcar function (rows matrix))))

(defun matrix-from-rows (&rest rows)
  "Make a matrix from the specified rows"
  (transpose (apply #'matrix-from-columns rows)))

(defun map-rows (function matrix)
  "Apply a function that returns a row for each of the rows of the matrix"
  (apply #'matrix-from-rows (mapcar function (rows matrix))))
